
import numpy as np
from matplotlib import pyplot as plt

from typing import Callable
from dataclasses import dataclass


@dataclass
class Position:
    x: float
    y: float
    theta_in_degree: float


def deg2rad(degree: float) -> float:
    return degree*np.pi/180


def create_path(q_i: Position, q_f: Position) -> tuple[Callable[[float], float], Callable[[float], float], Callable[[float], float]]:

    delta_x = q_f.x - q_i.x

    delta_y = q_f.y - q_i.y

    d_i = np.tan(deg2rad(q_i.theta_in_degree))
    d_f = np.tan(deg2rad(q_f.theta_in_degree))

    print(f'd_f:{d_f}, d_i {d_i}')

    a_0 = q_i.x
    a_1 = delta_x
    a_2 = 0
    a_3 = delta_x - a_2 - a_1

    b_0 = q_i.y
    b_1 = d_i*a_1
    b_2 = 3*delta_y - 3*d_f*delta_x + d_f*a_2 - 2*(d_i - d_f)*a_1
    b_3 = 3*d_f*delta_x - 2*delta_y - d_f*a_2 - (2*d_f - d_i)*a_1

    print(f' f(x) = {a_0} + {a_1}x + {a_2}x² + {a_3}x³')

    print(f' f(y) = {b_0} + {b_1}y + {b_2}y² + {b_3}y³')

    def fuc(a, b, c, d):
        return lambda x: a + b*x + c*x**2 + d*x**3

    def theta(t: float) -> float:
        return np.arctan((b_1 + 2*b_2*t + 3*b_3*t**2)/((a_1 + 2*a_2*t + 3*a_3*t**2)))

    return fuc(a_0, a_1, a_2, a_3), fuc(b_0, b_1, b_2, b_3), theta




def main():

    q_i = Position(0, 0, 0)

    q_f = Position(10, 10, 45)

    p_x, p_y, theta_t = create_path(q_i, q_f)

    t = np.arange(0.0, 1.0, 0.001)


    fig, ax = plt.subplots()
    ax.plot(p_x(t), p_y(t))

    ax.set(title='posição x,y do robô com $\lambda$ entre [0,1] ')
    ax.set_xlabel('$x(\lambda) = 10\lambda$')
    ax.set_ylabel('$y(\lambda) = 20 \lambda^{2} - 10 \lambda^{3}$', rotation=0, )
    ax.yaxis.set_label_coords(-0.1,0.5)
    ax.set_autoscale_on(True)
    
    # ax.plot(p_x(0.5), p_y(0.5))
    ax.grid()
    ax.scatter(p_x(0.5), p_y(0.5),c='g')
    
    plt.figure(2)
    fig, ax = plt.subplots()
    ax.plot(t, theta_t(t))
    ax.scatter(0.5, theta_t(0.5),c='g')
    ax.set(title='$\\theta(\lambda) = tan^{-1}( 4\lambda - 3\lambda^{2} )$ com $\lambda$ entre [0,1] ')
    plt.show()

    pass


if __name__ == '__main__':
    main()
